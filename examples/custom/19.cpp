#include <punk.hpp"

inline int myprocess(std::vector<int> x) {
    std::uniform_int_distribution<> dis(0, 199);
    return dis(gen);
}

inline Point mycombine(Point P, Point Q) {
    float k = 0.1;
    return P*k + Q*(1 - k);
}

inline Point f(double t) {
    return Point(std::pow(cos(t),3),
                 std::pow(sin(t), 3))*1e3;
}

int main (void) {
    PointSet X = sample<double>(2e2, f, 0, 2*PI);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-19.cpp")
         .draw(Y);
    return 0;
}

