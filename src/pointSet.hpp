#pragma once
#include <cmath>
#include <vector>
#include <random>
#include <functional>
#include <iostream>
#include "utils.hpp"

struct Point {
    float x;
    float y;

    // Constructor
    Point(float x = 0, float y = 0) : x(x), y(y) {};

    // Basic operations between points
    [[nodiscard]]
    bool operator== (const Point& q) const {
        return (x == q.x && y == q.y);
    }

    [[nodiscard]]
    bool operator!= (const Point& q) const {
        return (x != q.x || y != q.y);
    }

    [[nodiscard]]
    Point operator+ (const Point& q) const {
        return { x + q.x, y + q.y};
    }

    [[nodiscard]]
    Point operator- (const Point& q) const {
        return { x - q.x, y - q.y };
    }

    [[nodiscard]]
    Point operator* (float k) const {
        return { k*x, k*y };
    }

    [[nodiscard]]
    Point operator/ (float k) const {
        return { x/k, y/k };
    }

    [[nodiscard]]
    std::string to_string() const {
        return "(" + std::to_string(x) + ", " + std::to_string(y) + ")";
    }

    friend std::ostream& operator<< (std::ostream& os, const Point& p) {
        return os << p.to_string();
    }
};

/* Returns a random Point p whose coordinates p.x and p.y are random floats
   in [-x, x] and [-y, y], respectively. */
[[nodiscard]]
inline Point randomPoint(float x, float y) {
    std::uniform_real_distribution<> disx(-x, x);
    std::uniform_real_distribution<> disy(-y, y);
    return { (float) disx(gen), (float) disy(gen) };
}

/* Returns a float number corresponding to the modulus of a Point p */
[[nodiscard]]
inline float rho(Point p) {
    return std::hypot(p.x, p.y);
}

/* Returns a float corresponding to the argument of the Point p, this is
   the angle, in radians, formed by the x-axis and the line passing trough
   p and the origin. */
[[nodiscard]]
inline float theta(Point p) {
    return std::atan2(p.y, p.x);
}

/* Returns a Point q corresponding to the counter-clockwise rotation of p
   by some angle theta (in radians). */
[[nodiscard]]
inline Point rotate(const Point& p, float theta) {
    float cosine = cos(theta);
    float sine = sin(theta);
    return { p.y*cosine - p.x*sine,
             p.y*sine   + p.x*cosine };
}

// PointSet type
typedef std::vector<Point> PointSet;

/* Returns a PointSet Y whose elements correspond to the elements of the
   PointSet X, but shifted by the given offset q. */
[[nodiscard]]
inline PointSet translate(const PointSet& X, Point q) {
    PointSet Y(X.size());
    for (const Point& p : X) Y.emplace_back(p + q);
    return Y;
}

/* Returns a PointSet Y corresponding to the midpoints of each pair of
   consecutive points in the PointSet X. */
[[nodiscard]]
inline PointSet midpoints(const PointSet& X) {
    PointSet Y(X.size());
    Point q = X[0];
    std::vector<Point> rest(X.begin() + 1, X.end());

    for (Point& p : rest) {
        Y.emplace_back((q+p)/2);
        q = p;
    }
    Y.emplace_back((q + X[0])/2);
    return Y;
}

/* Returns a PointSet Y corresponding to the points of X, interleaved
   by it's midpoints. */
[[nodiscard]]
inline PointSet withMidpoints(const PointSet& X) {
    int n = X.size();
    PointSet Y;
    PointSet M = midpoints(X);
    for (int i = 0; i < n; i++) {
        Y.emplace_back(X[i]);
        Y.emplace_back(M[i]);
    }
    return Y;
}

/* Returns a PointSet Y consisting of the midpoints of all pairs of
   distinct points of X. */
[[nodiscard]]
inline PointSet allMidpoints(const PointSet& X) {
    int n = X.size();
    PointSet Y((int) n*(n - 1)/2);
    for (int i = 0; i < n; i++)
        for (int j = i + 1; j < n; j++)
            Y.emplace_back((X[i]+X[j])/2);
    return Y;
}

/* Returns a PointSet Y consisting of all points of X rotated by theta. */
[[nodiscard]]
inline PointSet rotate(const PointSet& X, float theta) {
    PointSet Y(X.size());
    for (const Point& p : X) Y.emplace_back(rotate(p, theta));
    return Y;
}

/* Returns a PointSet Y corresponding to the PointSet X, but with every Point
   scaled by the given float factor. */
[[nodiscard]]
inline PointSet scale(const PointSet& X, float factor) {
    PointSet Y(X.size());
    for (const Point& p : X) Y.emplace_back(p*factor);
    return Y;
}

/* Returns a PointSet Z corresponding to the union of the PointSet X and the
   PointSet Y, in that order. */
[[nodiscard]]
inline PointSet merge(const PointSet& X, const PointSet& Y) {
    PointSet Z;
    Z.insert(Z.end(), X.begin(), X.end());
    Z.insert(Z.end(), Y.begin(), Y.end());
    return Z;
}

/* Returns a sample of n points out of the image of f over [a, b]. */
template<typename T>
[[nodiscard]]
inline PointSet sample(int n,
                       std::function<Point(T)> f,
                       T a,
                       T b) {
    PointSet X(n);
    T h = (b - a)/n;
    for (int i = 0; i < n; i++) X[i] = f(a+i*h);
    return X;
}

/* Returns a PointSet X corresponding to a n-sided regular poligon
   inscribed in a circumference of radius r, which is centred in
   Point(0,0). */
[[nodiscard]]
inline PointSet regularPointSet(int n, float r) {
    auto f = [r](float t) { return Point(cos(t), sin(t))*r; };
    return sample<float>(n, f, 0, 2*PI);
}

/* Returns a random PointSet X corresponding to n-sided polygon
   inscribed in a circumference of radius r centred in Point(0,0). */
[[nodiscard]]
inline PointSet randomPointSet(int n, float r) {
    auto f = [r](float _) { 
        std::uniform_int_distribution<> dis(0, 359);
        int t = dis(gen);
        return Point(cos(t), sin(t))*r;
    };
    return sample<float>(n, f, 0, 0);
}

std::ostream& operator<< (std::ostream& os, const PointSet& X) {
    Point first = X.front();
    std::string str = first.to_string();

    std::vector<Point> rest(X.begin() + 1, X.end());
    for (Point& p : rest) str += ", " + p.to_string();
    return os << "{" << str << "}";
}
