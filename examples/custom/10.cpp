#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float> 
    transitionMatrix[6] = {
         {1, 1, 0, 0, 0, 1},
         {1, 1, 1, 0, 0, 0},
         {0, 1, 1, 1, 0, 0},
         {0, 0, 1, 1, 1, 0},
         {0, 0, 0, 1, 1, 1},
         {1, 0, 0, 0, 1, 1}};
    return weightedIndex(transitionMatrix[x[0]]);
}

inline Point mycombine(Point P, Point Q) {
    float k = -0.5;
    return P*k + Q*(1-k);
}

int main (void) {
    PointSet X = regularPointSet(6, 500);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-10.png")
         .draw(Y);
    return 0;
}

