#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::uniform_int_distribution<> dis(0, 2);
    return dis(gen);
}

inline Point T(Point P) {
    static int counter = -1;
    counter++;
    if (counter < 10) {
        return P;
    } else {
        counter = 0;
        return rotate(P, PI)*4;
    }
}

inline Point mycombine(Point P, Point Q) {
    float k = 0.5;
    return P*k + T(Q)*(1-k);
}

int main (void) {
    PointSet X = regularPointSet(3, 1000);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(2)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();
    Y = rotate(Y, PI/3);

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-13.png")
         .draw(Y);
    return 0;
}

