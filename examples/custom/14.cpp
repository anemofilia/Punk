#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::uniform_int_distribution<> dis(0, 2);
    return dis(gen);
}

inline Point T(Point P) {
    static int counter = -1;
    counter++;
    if (counter < 10) {
        return P;
    } else {
        counter = 0;
        return rotate(P, 180)*4;
    }
}

inline Point mycombine(Point P, Point Q) {
    float k = 0.5;
    return P*k + T(Q)*(1-k);
}

int main (void) {
    PointSet X = rotate(regularPointSet(3, 400), -PI/3);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-14.png")
         .draw(Y);
    return 0;
}
