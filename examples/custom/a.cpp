#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float>
    transitionMatrix[6][6] = {
        {{1, 0, 1, 1, 1, 0},
         {0, 1, 0, 1, 1, 1},
         {0, 0, 1, 0, 1, 1},
         {0, 1, 0, 1, 0, 1},
         {0, 1, 1, 0, 1, 0},
         {0, 1, 1, 1, 0, 1}},

        {{1, 0, 1, 1, 1, 0},
         {0, 1, 0, 1, 1, 1},
         {1, 0, 1, 0, 1, 1},
         {1, 0, 0, 1, 0, 1},
         {1, 0, 1, 0, 1, 0},
         {0, 0, 1, 1, 0, 1}},

        {{1, 0, 0, 1, 1, 0},
         {0, 1, 0, 1, 1, 1},
         {1, 0, 1, 0, 1, 1},
         {1, 1, 0, 1, 0, 1},
         {1, 1, 0, 0, 1, 0},
         {0, 1, 0, 1, 0, 1}},

        {{1, 0, 1, 0, 1, 0},
         {0, 1, 0, 0, 1, 1},
         {1, 0, 1, 0, 1, 1},
         {1, 1, 0, 1, 0, 1},
         {1, 1, 1, 0, 1, 0},
         {0, 1, 1, 0, 0, 1}},

        {{1, 0, 1, 1, 0, 0},
         {0, 1, 0, 1, 0, 1},
         {1, 0, 1, 0, 0, 1},
         {1, 1, 0, 1, 0, 1},
         {1, 1, 1, 0, 1, 0},
         {0, 1, 1, 1, 0, 1}},

        {{1, 0, 1, 1, 1, 0},
         {0, 1, 0, 1, 1, 0},
         {1, 0, 1, 0, 1, 0},
         {1, 1, 0, 1, 0, 0},
         {1, 1, 1, 0, 1, 0},
         {0, 1, 1, 1, 0, 1}}
    };
    return weightedIndex(transitionMatrix[x[0]][x[5]]);
}

PointSet X = regularPointSet(6, 3e2);

inline Point mycombine(Point P, Point Q) {
    // Scale factor dependent on the referencePoint choosen
    float k = (Q == X[0] || Q == X[2] || Q == X[4]) ? 0.5 : -0.4; 
    return P*k + Q*(1-k);
}

inline Point combine(Point P, Point Q) {
    // Scale factor dependent on the proximity between P and Q
    float k = (rho(P - Q) < 100) ? 0.5 : -0.5;
    return P*k + Q*(1-k);
}

int main() {
    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(6)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-a.png")
         .draw(Y);
    return 0;
}
