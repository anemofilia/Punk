#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::uniform_int_distribution<> dis(0, 9);
    return dis(gen);
}

inline Point T(Point P) {
    return rotate(P, PI/2);
}

inline Point mycombine(Point P, Point Q) {
    float k = 0.2;
    return T(P*k + Q*(1-k));
}

int main (void) {
    PointSet X = merge(regularPointSet(6, 1000),
                       regularPointSet(3, 500));

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-8.png")
         .draw(Y);
    return 0;
}
