#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    static unsigned k = 0;
    k = (k + 1)%3;
    std::vector<float>
    transitionMatrix[6] = {
         {0, 0, 1, 1, 1, 0},
         {0, 0, 0, 1, 1, 1},
         {1, 0, 0, 0, 1, 1},
         {1, 1, 0, 0, 0, 1},
         {1, 1, 1, 0, 0, 0},
         {0, 1, 1, 1, 0, 0}};
    return weightedIndex(transitionMatrix[x[k]]);
}

inline Point mycombine (Point P, Point Q) {
    float k = -0.48; 
    return P*k + Q*(1-k);
}

int main() {
    PointSet X = regularPointSet(6, 300);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(3)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-23.png")
         .draw(Y);
    return 0;
}
