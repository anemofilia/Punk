#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::uniform_int_distribution<> dis(0, 2);
    return dis(gen);
}

inline Point mycombine(Point P, Point Q) {
    return (P + Q)*(-0.5);
}

int main (void) {
    PointSet X = regularPointSet(3, 1000);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-16.png")
         .draw(Y);
    return 0;
}
