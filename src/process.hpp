#pragma once
#include "utils.hpp"

/* Returns a random integer from 0 to weights.size() - 1, with each index
    i having probability proportional to the value of weight[i]. */
[[nodiscard]]
inline int weightedIndex(const std::vector<float>& weights) {
    std::discrete_distribution<int> dis(weights.begin(), weights.end());
    return dis(gen);
}
