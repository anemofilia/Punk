#include "pointSet.hpp"
#include "process.hpp"
#include "utils.hpp"
#include <optional>
#include <chrono>
#include <functional>
#include <iostream>

/* Returns a PointSet Y corresponding to the result of n iterations
   of the Iterated Function System, using a transformation T over
   the given inital point. */
[[nodiscard]]
PointSet simulateIFS(int n,
                     std::function<Point(const Point&)> T,
                     Point initialPoint) {

    std::cout << "Simulating points..." << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    std::vector<Point> points(n);
    Point iterationPoint = initialPoint;

    for (int i = 0; i < n; ++i) {
        // Store it on simulate output
        points.emplace_back(iterationPoint);

        // update iterationPoint
        iterationPoint = T(iterationPoint);
    }
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "    "
              << "Elapsed simulation time: "
              << (double) std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count()/1000
              << "s" << std::endl;
    return PointSet(points);
}

class IFS {
public:
    IFS() {}

    IFS& setIterations(unsigned int iterations) {
        m_iterations = iterations;
        return *this;
    }

    IFS& setTransformation(std::function<Point(const Point&)> transformation) {
        m_transformation = transformation;
        return *this;
    }

    IFS& setInitialPoint(Point initialPoint) {
        m_initialPoint = initialPoint;
        return *this;
    }

    PointSet simulate(void) {
        return ::simulateIFS(m_iterations,
                             m_transformation,
                             m_initialPoint);
    }

private:
    unsigned int m_iterations = 1e6;
    std::function<Point(const Point&)> m_transformation;
    Point m_initialPoint = randomPoint(500,500);
};
