#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float>
    transitionMatrix[5][5] = {
        {{1, 0, 1, 1, 0},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1}},

        {{1, 1, 1, 1, 1},
         {0, 1, 0, 1, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1}},

        {{1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1},
         {1, 0, 1, 0, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1}},

        {{1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 0, 1, 0},
         {1, 1, 1, 1, 1}},

        {{1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1},
         {0, 1, 1, 0, 1}}
    };
    return weightedIndex(transitionMatrix[x[0]][x[1]]);
}

inline Point mycombine(Point P, Point Q) {
    float k = -sierpinskiFactor(5);
    return P*k + Q*(1-k);
}

int main (void) {
    PointSet X = regularPointSet(5, 400);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(2)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-3.png")
         .draw(Y);
    return 0;
}
