#include <punk.hpp>

typedef std::array<unsigned,6> dice;

std::vector<dice> die = {
    {36, 27, 20, 15,  8, 5},
    {35, 30, 22, 14,  7, 3}, 
    {34, 25, 21, 13, 12, 6}, 
    {33, 28, 19, 18,  9, 4}, 
    {32, 26, 23, 17, 11, 2},
    {31, 29, 24, 16, 10, 1}
};

inline unsigned championship(std::vector<dice> die) {
    std::uniform_int_distribution dis(0, 5);
    std::vector<unsigned> dice_throw;
    for (unsigned i = 0; i < 6; i++)
        dice_throw.emplace_back(die[i][dis(gen)]);

    unsigned champion = 0;
    for (unsigned i = 1; i < 6; i++)
        if (dice_throw[champion] < dice_throw[i])
           champion = i;
    return champion;
}

inline int myprocess(std::vector<int> x) {
    return championship(die);
}

inline Point mycombine (Point P, Point Q) {
    float k = sierpinskiFactor(6); 
    return P*k + Q*(1-k);
}

int main() {
    PointSet X = regularPointSet(6, 1000);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(3)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-26.png")
         .draw(Y);
    return 0;
}

