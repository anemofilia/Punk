#include "../../punk.hpp"

inline auto T (Point P) {
    std::vector<float> weights = {1, 85, 7, 7};
    unsigned int random = weightedIndex(weights);
    switch (random) {
        case 0:
           return Point(0, 0.16 * P.y);
        case 1:
           return Point( 0.85 * P.x + 0.04 * P.y,
                        -0.04 * P.x + 0.85 * P.y + 1.7);
        case 2:
           return Point( 0.20 * P.x - 0.26 * P.y,
                         0.23 * P.x + 0.22 * P.y + 1.7);
        case 3:
           return Point(-0.15 * P.x + 0.28 * P.y,
                         0.26 * P.x + 0.24 * P.y + 0.44);
    }
}

int main() {
    IFS myIFS;
    auto X = myIFS.setInitialPoint(Point(1000,1000))
                  .setTransformation(T)
                  .setIterations(1e8) 
                  .simulate();
    X = scale(X, 810);
    X = translate(X,Point(0,-4020));

    Image image;
    image.setPointColor(sf::Color(20,255,20))
         .setFilename("fern.png")
         .setWidth(10000)
         .setHeight(10000)
         .draw(X);
    return 0;
}

