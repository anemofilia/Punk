#include <punk.hpp>

template<typename T>
inline std::vector<T> rotate(int n, std::vector<T> slice) {
    for (int i = 0; i < n; i++) {
        slice.emplace_back(slice.front());
        std::vector<T> temp(slice.begin() + 1, slice.end());
        slice = temp;
    }
    return slice;
}

inline int myprocess(std::vector<int> x) {
    std::vector<float> vec;
    for (int i = 0; i < 100; i++) vec.emplace_back(0);
    for (int i = 0; i < 100; i++) vec.emplace_back(1);
    return weightedIndex(rotate<float>(x[0], vec));
}

inline Point mycombine(Point P, Point Q) {
    float k = sierpinskiFactor(200);
    return P*k + Q*(1 - k);
}

int main (void) {
    PointSet X = regularPointSet(20, 1000);
    for (int i = 1; i < 10; i++)
        X = merge(X, rotate(regularPointSet(20, 100*i), 36*i));

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-17.png")
         .draw(Y);
    return 0;
}
