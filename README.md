# Punk

**C++ package for fractal generation using the Chaos Game.**

Punk aims to be a easy to use, fast and hackable C++ library for constructing
fractals through Complex Systems like Chaos Game or the Iterated Function
System. It abstracts away many properties of these processes simply as function
arguments, so generating a fractal can be as simply as

```cpp
#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float> 
    transitionMatrix[5] = {
         {0, 1, 1, 1, 1},
         {1, 0, 1, 1, 1},
         {1, 1, 0, 1, 1},
         {1, 1, 1, 0, 1},
         {1, 1, 1, 1, 0}
    };
    return weightedIndex(transitionMatrix[x[0]]);
}

inline Point mycombine(Point P, Point Q) {
    float k = -sierpinskiFactor(5);
    return P*k + Q*(1-k);
}

int main (void) {
    PointSet X = regularPointSet(5, 400);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("fractal.png")
         .draw(Y);
    return 0;
}
```

and then compiling and running the program with
```
g++ -Ofast -fopenmp -lsfml-graphics program.cpp -o fractal-generator
./fractal-generator
imv fractal.png     # Replace imv by your image visualizer of choice
```
Where `SFML` is needed for the image rendering and `libomp` is optional, but
enables that rendering to be done in a parallel fashion.

# Design
A Chaos Game in Punk is simply a object
```cpp
class ChaosGame {
public:
    ChaosGame() {}

    ... // Setters for each one of the private attributes of a Chaos Game

    PointSet simulate() {
        return ::simulateChaosGame(m_iterations,
                                   m_referencePointSet,
                                   m_processOrder,
                                   m_process,
                                   m_combinationFunction,
                                   m_initialPoint);
    }

private:
    unsigned int m_iterations = 1e6;
    PointSet m_referencePointSet;
    unsigned int m_processOrder;
    std::function<int(std::vector<int>)> m_process;
    std::function<Point(const Point&, const Point&)> m_combinationFunction;
    Point m_initialPoint = randomPoint(500,500);
};
```

Arbitrary stochastic processes can be constructed and used to select the
reference points, it can have whatever order and number of states you like.
Many examples contained in examples/custom construct them through transition
tensors. That's not necessary though, the process can be whatever function
receiving a `std::vector<int>` of the process past states that returns a `int`
corresponding to the new process state.

The combination function corresponds to the rule used to combine the last
generated point `P` and the reference point `Q`, whose index in the
`referencePointSet` was selected through the process. This function can be
whatever function receiving two `Point` objects and returning another `Point`.
In that sense, what Punk provides is actually a generalization of the original
Chaos Game, where you would only specify the factor that would be used to get
the convex linear combination of the two points.

Similarly, a IFS is an object
```cpp
class IFS {
public:
    IFS() {}

    ... // Setters for each one of the private attributes of a IFS

    PointSet simulate(void) {
        return ::simulateIFS(m_iterations,
                             m_transformation,
                             m_initialPoint);
    }

private:
    unsigned int m_iterations = 1e6;
    std::function<Point(const Point&)> m_transformation;
    Point m_initialPoint = randomPoint(500,500);
};
```

Some construction decisions were made favoring a more functional style in the
library source code, since it enables some versatility like permiting the
library user to color the points on the fractal by any property, without need
to construct the entire point coloring function on its own. More features may
be added in the same spirit. That style shouldn't imply in any significative
overhead, neither force the library user to program in the same way. Feel free
to use the paradigm you are the most used to.

Punk does not provide any builtin image visualizer by default. That's intended,
since the user can use the one he prefers the most. If you think any feature,
including this one is desirable to be included, feel free to open an issue or
pull request it :).
