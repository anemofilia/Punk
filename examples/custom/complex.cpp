#include "../../punk.hpp"

inline int myprocess(std::vector<int> x) {
    std::uniform_int_distribution<> dis(0, 5);
    return dis(gen);
}

inline Point mycombine (Point P, Point Q) {
    float k = 0.1; 
    return P*k + Q*(1-k);
}

int main() {
    PointSet X = regularPointSet(6, 1000);
    ChaosGame G;
    PointSet Y = G.setIterations(1e9)
                  .setReferencePointSet(X)
                  .setProcessOrder(10)
                  .setProcess(myprocess)
                  .setCombinationFunction(mycombine)
                  .simulate();
    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-25.png")
         .draw(Y);
    return 0;
}

