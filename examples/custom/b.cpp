#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::uniform_int_distribution<> dis(0, 5);
    return dis(gen);
}

PointSet X = regularPointSet(6, 500);

inline Point mycombine(Point P, Point Q) {
    // Scale factor dependent on the referencePoint choosen
    float k = (Q == X[0] || Q == X[2] || Q == X[4]) ? 0.32 : -0.32; 
    return P*k + Q*(1-k);
}

inline float d(Point P, Point Q) {
    return std::max(std::abs(P.x - Q.x),
                    std::abs(P.y - Q.y));
}

inline bool near(float k, Point p, PointSet X) {
    for (Point& q : X) if (rho(p - q) < k) return true;
    return false;
}

inline Point combine(Point p, Point q) {
    // Scale factor dependent on the argument of the vector defined by P and Q
    float k = (near(150, p, X)) ? 0.3 : -0.5;
    return p*k + q*(1-k);
}

int main() {
    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(6)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-b.png")
         .draw(Y);
    return 0;
}
